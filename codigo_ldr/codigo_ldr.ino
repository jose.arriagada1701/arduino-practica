/*
 *    Por José Arriagada
 *    Leer la distancia del sensor en cm
 */

const int PIN = 8;

void setup() {
  Serial.begin(9600);
  pinMode(PIN,OUTPUT);
}

void loop() {
  digitalWrite(8,HIGH);
  int out = analogRead(A0);
  Serial.print(out);
  Serial.println("cm");
  delay(1000);
}
