  /*
   * Por José Arriagada
   * version final del programa maquina mezcladora, es similar pero con otro sensor para profundidad
  */
  
  #include <Servo.h>
  
  Servo myServo1;
  Servo myServo2;
  const int servo1 = 9;//derecho
  const int servo2= 10;// izquierdo
  
  const int Trigger1 = 12;   // derecho
  const int Echo1 = 13;   //
  const int Trigger2 = 3;   // izquierdo
  const int Echo2 = 2;   //
  const int Elleno = 13; //número estanque lleno
  
  void setup() {
    myServo1.attach(servo1);//derecho
    myServo2.attach(servo2);//izquierdo
    
    pinMode(Trigger1, OUTPUT); //pin como salida
    pinMode(Echo1, INPUT);  //pin como entrada
    digitalWrite(Trigger1, LOW);//Inicializamos el pin con 0
    
    pinMode(Trigger2, OUTPUT); //pin como salida
    pinMode(Echo2, INPUT);  //pin como entrada
    digitalWrite(Trigger2, LOW);//Inicializamos el pin con 0
    Serial.begin(9600);   
  }
  
  void mover(int ts1, int ts2){
  
  
      Serial.println("Empezando a mover los motores");
      Serial.println("Inicio motor derecho");
      moverServo1(180,ts1);
      Serial.println("Fin motor derecho");
      
      Serial.println("Incio motor izquierdo");
      moverServo2(180,ts2);
      Serial.println("Fin motor izquierdo");
      Serial.println("Fin de mover los motores");
  
      
    }
  void moverServo1(int a,int ts){
      ts*=60;
      myServo1.write(a);
      delay(ts*1000); 
      myServo1.write(90);
      delay(1000);
    }
  void moverServo2(int a,int ts){
      ts*=60;
      myServo2.write(a);
      delay(ts *1000); 
      myServo2.write(90);
      delay(1000); 
    }
  long readUltrasonicDistance(int triggerPin, int echoPin)
  {
  
    pinMode(triggerPin, OUTPUT);  // Clear the trigger
    digitalWrite(triggerPin, LOW);
    delayMicroseconds(2);
  
    // Sets the trigger pin to HIGH state for 10 microseconds
   
   digitalWrite(triggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(triggerPin, LOW);
    pinMode(echoPin, INPUT);
    
  // Reads the echo pin, and returns the sound wave travel time in microseconds
    return pulseIn(echoPin, HIGH);
  }
  
  void loop() {
  
    int ts1=1;
    int ts2=1;
    
    int b =1;
    Serial.println("Estanque derecho[min]:");
    while(b==1){
    if(Serial.available()){
      String result = Serial.readStringUntil('\n');
      ts1 = result.toInt();
      Serial.print(ts1);
      Serial.println(" [min]");
      b=0;
      if(ts1 <1)
        b=1;
      }
    }
    b=1;
    Serial.println("Estanque izquierdo[min]: ");
    while(b==1){
    if(Serial.available()){
      String result = Serial.readStringUntil('\n');
      ts2 = result.toInt();
      Serial.print(ts2);
      Serial.println(" [min]");
      b=0;
      if(ts2 <1)
        b=1;
      }
    }
    
    long t1; //tiempo que demora en llegar el eco
    long d1; //distancia en centimetros
    long t2; //tiempo que demora en llegar el eco
    long d2; //distancia en centimetros
  
    
    
    
    delay(100); 
    
    int band=1;
    while(band==1){
      d1 = 0.01723 * readUltrasonicDistance(3, 2);
      d2 = d1;
      Serial.print("Nivel derecho: ");
      Serial.println(d1);
      Serial.print("Nivel izquierdo: ");
      Serial.println(d2);
        //if(d1==Elleno && d2 == Elleno){
        if(d2 <= Elleno){
          Serial.println("Estan Llenos...");
          mover(ts1,ts2);
          int b=1;
          Serial.println("Reiniciar o [1]");
          while(b==1){
            if(Serial.available()){
              String result = Serial.readStringUntil('\n');
              int resp = result.toInt();
              Serial.print("respuesta: ");
              Serial.println(resp);
            if(resp==1){
              b=0;
              Serial.println("Volviendo a empezar");
              band=0;
              }
            else
              Serial.println("Reiniciar o [1]");
            }
          }
        }
      else{
        
        if(d1 == Elleno)
          Serial.println("Estanque derecho está lleno");
        else{
          Serial.print("Nivel derecho: ");
          Serial.println(12-d1);
          }
        
        if(d2 == Elleno){
          Serial.println("Estanque Izquierdo lleno");
          }
        else
          Serial.print("Nivel izquierdo: ");  
          Serial.println(12-d2);
      }
      delay(800);
      
    }
  }
    
