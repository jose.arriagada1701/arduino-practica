/*
 * Por José Arriagada
 * Calcula en cm la distancia que mide el sensor
 */
const int PIN=8;
const int T=1;//debe ser un n entero

//obtenido de internet 
long readUltrasonicDistance(int triggerPin, int echoPin)
{

  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);

  // Sets the trigger pin to HIGH state for 10 microseconds
 
 digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  
// Reads the echo pin, and returns the sound wave travel time in microseconds
  return pulseIn(echoPin, HIGH);
}

void setup()
{
  Serial.begin(9600);

}

void loop()
{
  //convertir a cm 
  int cm = 0.01723 * readUltrasonicDistance(3, 2);
  Serial.print(cm);
  Serial.println("cm");
  delay(T); // espero t segundos
}
