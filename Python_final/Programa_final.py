import tkinter as tk
from threading import Thread
import time
import serial
from datetime import datetime
from PIL import Image, ImageTk
"""
Desarrollado Por 
José Arriagada
Practica 1 CIMUBB
PUERTA EN LINUX /dev/ttyUSB0
WIN-> COM5 o COM7

"""
class GUI(object):
    def __init__(self):
        contenedor = tk.Frame(root) 
        self.block = 0
        self.a = "Bienvenido a la Mezcladora" 
        Font_text = ("Helvetica", 17, "bold")
        Font_text2 = ("Helvetica", 12, "bold")
        self.w = tk.Label(contenedor, text=self.a, font = Font_text2)
        self.w.pack()
        self.t1 = tk.Label(contenedor, text="Puerta", font = Font_text)
        self.t1.pack()
        self.entry1 = tk.Entry(contenedor, justify='center')
        self.entry1.insert(0, "COM5")
        self.entry1.pack()
        self.t2 = tk.Label(contenedor, text="Recipiente Derecho[min]", font = Font_text)
        self.t2.pack()
        self.entry2 = tk.Entry(contenedor, justify='center')
        self.entry2.pack()
        self.t3 = tk.Label(contenedor, text="Recipiente Izquierdo[min]", font = Font_text)
        self.t3.pack()
        self.entry3 = tk.Entry(contenedor, justify='center')
        self.entry3.pack()
        self.iniciar= tk.Button(contenedor  , text="Iniciar/Reiniciar", command=self.hilo)
        self.iniciar.pack()
        
        contenedor.place(x=200,y=50)
        
    def hilo(self):
        
        if(not (self.entry2.get().isdigit() and self.entry3.get().isdigit())):
                self.a ="Datos no validos"
                root.after_idle(self.update)
                
                return
        if(self.block == 0):
            self.block = 1
            thread2 = Thread( target=self.arduino_hilo, args=("Thread-2", ) )
            thread2.start()
        else:
            self.a ="Iniciar bloqueado"
            root.after_idle(self.update)
        
    def arduino_hilo(self,threadname):
        archivo = open("resultado.txt", "a")
        while True:
            try:
                serialArduino =serial.Serial(self.entry1.get(), 9600)
            except:
                msj = "No valida, "+self.entry1.get()
                print(msj)
                archivo.write(msj)
                self.a =msj
                root.after_idle(self.update)
                self.block = 0
                return
                
            #Para el tiempo estanque derecho
            #leo del arduino, lo imprimó por consola, lo
            #muestro por pantalla y lo escribo en un archivo
            cad = serialArduino.readline().decode('utf-8')
            print(cad)
            archivo.write(str(datetime.now()) + " " + cad)
            self.a =cad
            root.after_idle(self.update) 
            #espero y escribo el valor del estanque derecho en
            # el arduino
            time.sleep(2)
            serialArduino.write(self.entry2.get().encode('utf-8'))
            #Digo por pantalla que esta escrito, lo imprimo por consola y
            #lo escribo en el archivo
            var = "Escribiendo valor derecho" + self.entry2.get()
            self.a =var
            root.after_idle(self.update) 
            print(var)
            archivo.write(str(datetime.now()) + var)
            #Lo mismo pero para para el motor izquierdo
            
            
            cad = serialArduino.readline().decode('utf-8')
            print(cad)
            archivo.write(str(datetime.now()) + " " + cad)
            self.a =cad
            root.after_idle(self.update) 
            
            time.sleep(2)
            serialArduino.write(self.entry2.get().encode('utf-8'))
            
            var = "Escribiendo valor Izquierdo" + self.entry2.get()
            self.a =var
            root.after_idle(self.update) 
            print(var)
            archivo.write(str(datetime.now()) + var)
    
            while(1):
            
                cad = serialArduino.readline().decode('utf-8')
                print(cad)
                archivo.write(str(datetime.now()) + " " + cad)
                self.a =cad
                root.after_idle(self.update)
                time.sleep(1)
                
                if(cad.find("[1]")!=-1):
                    print("Terminado el hilo")
                    archivo.write(str(datetime.now()) + "Terminado el hilo")
                    self.block = 0
                    return
                    
    def update(self):
        self.w.config(text=self.a)

if __name__ == "__main__":
    root = tk.Tk()
    root.geometry('800x500')
    root.title("Mezcladora Arduino")
    root.resizable(0,0)
    # Cargar el archivo de imagen desde el disco.
    icono = tk.PhotoImage(file="icon.png")
    # Establecerlo como ícono de la ventana.
    root.iconphoto(True, icono)
    python_image = ImageTk.PhotoImage(file='fondo_mezcladora2.jpg')
    fondo = tk.Label(root, image=python_image)
    fondo.place(x=0,y=0)
    gui = GUI()
    root.mainloop()
    
