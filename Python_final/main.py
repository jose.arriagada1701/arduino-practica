"""
Por José Arriagada

main del programa 

para controlar mezcladora

"""

from tkinter import Tk, Button, Label, PhotoImage, Entry
from PIL import Image, ImageTk
import serial
from datetime import datetime
import os 
import time
import threading

label_text = Label()
threads = list()
caja_puerta = Entry()
caja_tiempo1 = Entry()
caja_tiempo2 = Entry()

def capturar_datos(puerta):
    try:
        serialArduino =serial.Serial(puerta, 9600)
    except:
        print("[Error 1]: Puerta Arduino, " + puerta + " no es valida" )
        return
    time.sleep(1)
    cad = serialArduino.readline().decode('utf-8')
    #label_text.config(cad)
    print(cad)
    
    serialArduino.write("1".encode('utf-8'))
    #label_text.config("Escribiendo 1")
    print(cad)
    while(1):
        cad = serialArduino.readline().decode('utf-8')
        print(cad)
        label_text.config(cad)
        

def crear_ventana():
    ventana = Tk() 
    ventana.title("Mezcladora_Arduino-José_A")
    ventana.geometry('800x500')
    Font_text = ("Helvetica", 20, "bold")
    label_text = Label(ventana, text="",font= Font_text)
    label_text.pack()
    inicio = Button(ventana,text="Iniciar", command=iniciar_hilo_arduino)
    label_text1 = Label(ventana, text="Puerta",font= Font_text)
    label_text1.pack()
    caja_puerta = Entry(ventana)
    caja_puerta.pack()
    label_text2 = Label(ventana, text="Tiempo Derecho",font= Font_text)
    label_text2.pack()
    caja_tiempo1 = Entry(ventana)
    caja_tiempo1.pack()
    label_text2 = Label(ventana, text="Tiempo Izquierdo",font= Font_text)
    label_text2.pack()
    caja_tiempo2 = Entry(ventana)
    caja_tiempo2.pack()
    inicio.pack()
    ventana.mainloop()

def iniciar_hilo_arduino():
    
    print(caja_puerta.get())
    t1 = threading.Thread(target=capturar_datos, args=("",))
    threads.append(t1)
    t1.start()
    
if __name__ == '__main__':
    
    
    
    
    t2 = threading.Thread(target=crear_ventana)
    threads.append(t2)
    t2.start()

    
    
    
