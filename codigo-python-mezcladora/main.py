"""
Por José Arriagada

main del programa 
se crea la interfaz y se crear un "hilo" que va guadando los datos por detras en txt
cuando se apreta el boton se capturan los datos del arduino

lo modifique un poco para que no se caiga cuando se abre, es necesario que este conectado un
arduino para que funcione el programa.

"""

from tkinter import Tk, Button, Label, PhotoImage, Entry
from PIL import Image, ImageTk
import serial
from datetime import datetime
import os 
import time

com = "COM8"

ventana = Tk() 
ventana.geometry('800x500')


Font_title = ("Helvetica", 25, "bold")
Font_text = ("Helvetica", 20, "bold")
Font_b = ("Helvetica", 17, "bold")



#image = Image.open('fondo800.jpg')
#python_image = ImageTk.PhotoImage(image)

#l = Label(ventana, image=python_image)
#l.place(x = 0, y = 0) 
#l.pack(side = "bottom", fill = "both", expand = "yes")

l_title = Label(ventana, text = 'Mezcladora Arduino', font = Font_title).pack()


entry = Entry(ventana, font = Font_text)
parametros = Label(ventana, text = "Ingrese parametros", font = Font_text)


def capturar_datos():
    hw_sensor =serial.Serial(com, 9600)
    while True:
        hw_sensor.write('getValue'.encode('utf-8'))
        time.sleep(1)
        try:
            raw_string_b = hw_sensor.readline()
            raw_string_s = raw_string_b.decode('utf-8')
            if(raw_string_s.index("}")>=0 and raw_string_s.index("{")==0):
                raw_string_s = raw_string_s[0:raw_string_s.index("}")+1]
                raw_string_j = json.loads(raw_string_s)
                print(raw_string_j)
                print(raw_string_j["Sensor_id"])
                print(raw_string_j["Value"])
            else:
                print("error/ no } found.")
        except:
            print("Exception occurred, somthing wrong...")
    hw_sensor.close()

iniciar = Button(ventana, text = "Iniciar maquina",command = capturar_datos, font = Font_text)
parametros.pack()
entry.pack()
iniciar.pack()

ventana.mainloop()