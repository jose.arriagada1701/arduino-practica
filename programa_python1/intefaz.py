"""
Por José Arriagada

main del programa 
se crea la interfaz y se crear un "hilo" que va guadando los datos por detras en txt
cuando se apreta el boton se capturan los datos del arduino

lo modifique un poco para que no se caiga cuando se abre, es necesario que este conectado un
arduino para que funcione el programa.

"""

from tkinter import Tk, Button, Label, PhotoImage
from PIL import Image, ImageTk
import serial
from datetime import datetime
import os 




hora = 'Consulta: '
temp = 'Temperatura: '  
humd = 'Humedad :'
dist = 'Distancia: '
luz = 'Luz:'




ventana = Tk() 
ventana.geometry('800x500')


Font_title = ("Helvetica", 25, "bold")
Font_text = ("Helvetica", 20, "bold")
Font_b = ("Helvetica", 17, "bold")



image = Image.open('fondo800.jpg')
python_image = ImageTk.PhotoImage(image)

l = Label(ventana, image=python_image)
l.place(x = 0, y = 0) 
#l.pack(side = "bottom", fill = "both", expand = "yes")

l_title = Label(ventana, text = 'Datos Capturados', font = Font_title).pack()

l_hora = Label(ventana, text = hora, font = Font_text)
l_temp = Label(ventana, text = temp, font = Font_text)
l_humd = Label(ventana, text = humd, font = Font_text)
l_dist = Label(ventana, text = dist, font = Font_text)
l_luz = Label(ventana, text = luz, font = Font_text)

def capturar_datos():
    serialArduino =serial.Serial("COM8", 9600)
    archivo = open("resultado.txt", "a")
    cad = serialArduino.readline().decode('utf-8')
    print(cad)
    archivo.write(str(datetime.now()) + " " + cad)
    archivo.close()
    newcad = cad.split(';')
    print(newcad)
    l_hora.config(text= hora + str(datetime.now()))
    l_humd.config(text=newcad[0])
    l_temp.config(text=newcad[1])
    l_dist.config(text=newcad[2])
    l_luz.config(text=newcad[3])

l_hora.pack()
l_temp.pack()
l_humd.pack()
l_dist.pack()
l_luz.pack()
b_capt = Button(ventana,text = 'Actualizar Datos' ,font=Font_b, command=capturar_datos).pack()
b_quit = Button(ventana,text = 'Salir' ,font=Font_b, command = ventana.quit).pack()



#capturar_datos()
#os.system("start extraer.py &")
ventana.mainloop()





