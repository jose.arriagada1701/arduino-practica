# Practica 1 en  CIMUBB, Entre Agosto y Enero del 2023 por José Arriagada

El proyecto del LABORATORIO DE SISTEMAS INTEGRADOS DE MANUFACTURA – CIMUBB de la Universidad del Bío-Bío consistió en trabajar en una maqueta de una máquina mezcladora de productos industriales, utilizando Arduino para el control del hardware y software. La maqueta integra motores para el mezclado, sensores de temperatura y nivel, y actuadores, controlados por un Arduino Uno. El software desarrollado en Arduino IDE gestiona el proceso de mezclado automatizado.

** *En síntesis el proyecto es básicamente es una placa de Arduino conectada con componentes que ven la profundidad del liquido en los dos recipientes y dos motores Servo abren o cierran el paso del liquido (mezcladora.jpg contiene una captura de la maqueta)* **

-Contiene un respaldo de mis **códigos** de la práctica (En **Arduino y Python**).

-Contiene respaldo de **Informes** entregados(**Centro práctica y Lugar de Práctica**)

**OBS:**

-La carpeta **/programa_python1** incluye **primer programa realizado**, es para registrar la actividad una placa Arduino con sensores.

-La carpeta **/Python_Final** incluye el proyecto final, es para el control de una maqueta mezcladora por una **interfaz gráfica**. En la carpeta **/programav4-maquina_mezcladora** es el controlador de la maqueta en **Arduino**. Juntando todo se construye el proyecto final, está documentado en el INFORME para el lugar de práctica. 

-El archivo **Nivelación_Arduino.pdf**, es el material que se me entrego para aprender sobre Arduino.

-Además se añade: compilación **completa de las bitácoras**, los **formularios de inscripción de la práctica 1 y 2**, Las librerías requeridas en Arduino en **/LIBRERIAS**, también un **manual de Arduino** tomado de internet, y por ultimo un compendio de códigos en bruto de Arduino: estos fueron editados y escritos en la raíz principal pero algunos por su simpleza no (Para más en **/Codigos_en_bruto**).

*- Como dato extra  o adicional el largo de los recipiente de líquidos es de 12cm según el sensor de profundidad. En pruebas hechas se tardaba alrededor de 22 minutos, un estanque solo.*

![Captura Maquina Mezcladora](mezcladora.jpg)

Para mas información vease el [informe]([INFORME]Prática1_Para_Centro_Prática-José_Arriagada.pdf)


