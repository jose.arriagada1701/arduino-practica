/*
  Creado: Luis del Valle (ldelvalleh@programarfacil.com)
  https://programarfacil.com
*/

// Incluímos la librería para poder controlar el servo
#include <Servo.h>

// Declaramos la variable para controlar el servo
Servo servoMotor;

void setup() {
  // Iniciamos el monitor serie para mostrar el resultado
  

  // Iniciamos el servo para que empiece a trabajar con el pin 9
  servoMotor.attach(7);
  Serial.begin(9600);
}

void loop() {


  if(Serial.available()>0 ){
      int data = Serial.parseInt();

      Serial.println((int)data);

     servoMotor.write(data);
          delay(1000);
          servoMotor.write(90);
        
     
      
    
  }
  
  
}
