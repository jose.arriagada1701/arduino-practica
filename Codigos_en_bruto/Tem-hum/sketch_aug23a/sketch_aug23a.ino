/*
 * José Arriagada
 * Codigo chip DHT, sensor humedad
 */

#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN 7
#define DHTTYPE DHT11
DHT dht (DHTPIN,DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {

  delay(1000);

  float humedad = dht.readHumidity();
  float temperatura = dht.readTemperature();

  Serial.print("Humedad ");
  Serial.print(humedad);
  Serial.print("m3");
  Serial.print(" temperatura ");
  Serial.print(temperatura);
  Serial.print("°C");
  Serial.println();
}
