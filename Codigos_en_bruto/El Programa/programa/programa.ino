/*
 * Por José Arriagada
 * Primera version de programa de la maquina mezcladora
 */

// include the servo library
#include <Servo.h>

Servo myServo1;
Servo myServo2; // create a servo object 


void setup() {
  myServo1.attach(3);//derecho
  myServo2.attach(6);//izquierdo
  pinMode(2,OUTPUT);
  pinMode(5,OUTPUT);
  Serial.begin(9600); // open a serial connection to your computer
}

void mover(){
  
    
  }
void moverServo1(int a,int ts){
    myServo1.write(a);
    delay(ts*1000); 
    myServo1.write(90);
    delay(ts*1000); 
  }
void moverServo2(int a,int ts){
    myServo2.write(a);
    delay(ts*1000); 
    myServo2.write(90);
    delay(ts*1000); 
  }

void loop() {

  digitalWrite(2,HIGH);
  digitalWrite(5,HIGH);
  int ldrizq = analogRead(A0);
  int ldrder = analogRead(A1);
  int senco2 = analogRead(A2);


  Serial.print("ldrizq ");
  Serial.println(ldrizq);
  delay(1500);


  Serial.print("ldrder ");
  Serial.println(ldrder);
  delay(1500);
  

/*
  Serial.print("Co2 ");
  Serial.println(senco2);
  delay(1500);
  */

  if(ldrizq>14){
    moverServo2(0,1);
    }
  }
    
  
  
