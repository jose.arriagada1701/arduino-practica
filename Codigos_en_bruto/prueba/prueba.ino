#include <Servo.h>
#define DEBUG(a) Serial.println(a);
Servo myservo; 

void setup()
{
   Serial.begin(9600);
   myservo.attach(9);  // (pin, min, max)
}


void loop()
{

  int numb = 1000;
   if (Serial.available())
   {
      String data = Serial.readStringUntil('\n');
     // DEBUG(data);
     if (data.equals("R")){
        Serial.print("EN SENTIDO R");
        Serial.println();
        myservo.write(0);              
        delay(numb);
        myservo.write(90); 
      }
     if (data.equals("L")){
        Serial.print("EN SENTIDO L");
        Serial.println();
        myservo.write(180);              
        delay(numb);
        myservo.write(90); 
      }
      
   }
}
