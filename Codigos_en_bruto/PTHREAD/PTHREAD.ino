#include <ULWOS2.h>
/*
  ULWOS2 - Intermitente LED de funcionamiento de un solo hilo en Arduino
  Autor: Fabio Pereira
  Visite www.embeddedsystems.io para obtener más información sobre ULWOS2
*/
// Este es nuestro hilo, parpadea el LED incorporado sin código de bloqueo
void thread1 ( void ) 
{
  ULWOS2_THREAD_START () ;    // ¡esto es necesario para cada subproceso en ULWOS2!
  while ( 1 )
  {
    Serial.println("Hilo...");
    ULWOS2_THREAD_SLEEP_MS(1500);    // dormir por 500ms
  }
}  
void setup () {  
  ULWOS2_INIT () ;    // inicializa ULWOS2
  ULWOS2_THREAD_CREATE ( thread1, 1 ) ;    // crea hilo 1 con prioridad 10
  Serial.begin(9600);
}
void loop () {  
  // ejecutar el programador ULWOS2 (ejecutará todos los subprocesos que estén listos para ejecutarse)
  ULWOS2_START_SCHEDULER () ;
  Serial.println("Loop..");
}
