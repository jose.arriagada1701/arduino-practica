#define sensor A5 // Sharp IR GP2Y0A41SK0F (4-30cm, analog)

void setup()
{  Serial.begin(9600);
}

void loop()
 {
   float volts = analogRead(sensor)*0.0048828125;  // value from sensor * (5/1024)
   delay(100); // slow down serial port
   Serial.println(volts );     
}
