/*
  Creado: Luis del Valle (ldelvalleh@programarfacil.com)
  https://programarfacil.com
*/

// Incluímos la librería para poder controlar el servo
#include <Servo.h>

// Declaramos la variable para controlar el servo
Servo servoMotor;

void setup() {
  // Iniciamos el monitor serie para mostrar el resultado
  

  // Iniciamos el servo para que empiece a trabajar con el pin 9
  servoMotor.attach(7);
  Serial.begin(9600);
}

void loop() {


  if(Serial.available() > 0 ){
      char Dato = Serial.read();
      Serial.println(Dato);
      if(Dato == 'R'){
          Serial.println("Sentido2");
          servoMotor.write(0);
          delay(1000);
          servoMotor.write(90);
        }
       if(Dato == 'L'){
          Serial.println("Sentido1");
          servoMotor.write(0);
          delay(1000);
          servoMotor.write(90);
        }
     
      
    
  }
  
  
}
