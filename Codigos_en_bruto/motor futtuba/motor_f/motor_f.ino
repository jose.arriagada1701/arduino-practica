// include the servo library
#include <Servo.h>
String result;
Servo myServo;  // create a servo object 

int val;  // variable to read the value from serial monitor
int pos; 
int angle;   // variable to hold the angle for the servo motor 
String servoDirection;

void setup() {
  myServo.attach(7); // attaches the servo on pin 9 to the servo object 
  Serial.begin(9600); // open a serial connection to your computer
}

void mover(){
  
    result = Serial.readStringUntil('\n');
    int a = result.toInt();
    Serial.print("Moviendo en angulo: ");
    Serial.println(a);
    myServo.write(a);
    delay(500); 
    myServo.write(90);
    delay(500); 
  }

void loop() {

  if(Serial.available()){

    mover();
  }
  }
    
  
  
