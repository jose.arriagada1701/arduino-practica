/*
 * Por José Arriagada
 * Recibe por entrada el angulo en que debe moverse
 */

// include the servo library
#include <Servo.h>


Servo myServo; 
const int PIN=3;

void setup() {
  myServo.attach(PIN); 
  Serial.begin(9600); 
}

void mover(){
  
    String result = Serial.readStringUntil('\n');
    int a = result.toInt();
    Serial.print("Moviendo en angulo: ");
    Serial.println(a);
    myServo.write(a);
    delay(1000); 
    myServo.write(90);
    delay(500); 
}

void loop() {

  if(Serial.available()){
    mover();
  }
}
    
  
  
