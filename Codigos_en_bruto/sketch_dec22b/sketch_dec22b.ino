#include "pt.h"


pt pt1;
pt pt2;
#ifndef __PT_SLEEP_H__
#define __PT_SLEEP_H__

#define PT_SLEEP(pt, delay) \
{ \
  do { \
    static unsigned long protothreads_sleep; \
    protothreads_sleep = millis(); \
    PT_WAIT_UNTIL(pt, millis() - protothreads_sleep > delay); \
  } while(false); \
}
#endif /* __PT_SLEEP_H__ */


int hilo1(struct pt* pt)
{
  PT_BEGIN(pt);
 
  while(true)
  {
  Serial.println("Hilo..1");
  PT_SLEEP(pt, 250);
  }

  PT_END(pt);
}

int hilo2(struct pt* pt)
{
  PT_BEGIN(pt);
 
  while(true)
  {
  Serial.println("Hilo..2");
  PT_SLEEP(pt, 250);
  }

  PT_END(pt);
}

void setup()
{
  PT_INIT(&pt1);
  PT_INIT(&pt2);
  Serial.begin(9600);
}

void loop()
{
  PT_SCHEDULE(hilo1(&pt1));
  PT_SCHEDULE(hilo2(&pt2));
  while(1)
    Serial.println("Loop..");
  //  delay(500);
}
