#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN 7
#define DHTTYPE DHT11
DHT dht (DHTPIN,DHTTYPE);

int cm = 0;
long readUltrasonicDistance(int triggerPin, int echoPin)
{

  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);

  // Sets the trigger pin to HIGH state for 10 microseconds
 
 digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  
// Reads the echo pin, and returns the sound wave travel time in microseconds
  return pulseIn(echoPin, HIGH);
}

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {

  delay(1000);

  float humedad = dht.readHumidity();
  float temperatura = dht.readTemperature();

  Serial.print("Humedad: ");
  Serial.print(humedad);
  Serial.print(" m3;");
  Serial.print(" temperatura: ");
  Serial.print(temperatura);
  Serial.print(" °C; ");
  cm = 0.01723 * readUltrasonicDistance(8, 8);
  Serial.print(" Distancia: ");
  Serial.print(cm);
  Serial.print(" cm; ");
  
  int v = analogRead(A1);
  Serial.print("Luz: ");
  Serial.print(v);
  Serial.print(" Lx;");

  Serial.println();

  
  

}
