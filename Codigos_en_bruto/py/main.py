from tkinter import Tk, Label, Button
import extraer
from threading import *


class VentanaEjemplo:
    def __init__(self, master):
        self.master = master
        master.title("Una simple interfaz gráfica")
        master.geometry('700x550')
        master.configure(bg='#DCDEF0')
        self.etiqueta = Label(master, text=extraer.capturar_datos(),bg='#DCDEF0' )
        self.etiqueta.pack()
        self.botonSaludo = Button(master, text="Capturar Datos", command=self.saludar)
        self.botonSaludo.pack()
        self.botonCerrar = Button(master, text="Cerrar", command=master.quit,bg='red')
        self.botonCerrar.pack()
       
    def saludar(self):
         self.etiqueta['text'] = extraer.capturar_datos() 


root = Tk()
miVentana = VentanaEjemplo(root)
root.mainloop()