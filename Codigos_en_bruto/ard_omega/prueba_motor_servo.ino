/*
  Por José Arriagada
  Mover un motor Servo
*/

// Incluímos la librería para poder controlar el servo
#include <Servo.h>

Servo servoMotor;
const int PIN = 7;
void setup() {
  servoMotor.attach(PIN);
  Serial.begin(9600);
}

void loop() {


          Serial.println("Moviendo...");
          servoMotor.write(0);
          delay(1000);
          servoMotor.write(90);
          delay(1000);  
}
