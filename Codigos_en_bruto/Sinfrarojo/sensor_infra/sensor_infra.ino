int alto = 515, bajo = 90; //valores de tensión del sharp
int cm = 0;                //Para almacenar el valor obtenido en cm valor=0 
int sharp = 10;            //Entrada analógica del sensor.
int led = 10;
int valor;

void setup(){

  pinMode(led, OUTPUT);
  Serial.begin(9600);

}

void loop(){

  valor = analogRead(sharp);
  if (valor > alto || valor < bajo) Serial.println("OUT"); //fuera de rango
  else{    
    cm = (6787 / (valor - 3)) - 4;      //calculo
    Serial.println(cm); //cm detectados por el monitor
    //Si el objeto esta a menos de 20cm
    if(cm < 20) digitalWrite(led,HIGH);
    delay(600);
    digitalWrite(led,LOW);
  }
}
