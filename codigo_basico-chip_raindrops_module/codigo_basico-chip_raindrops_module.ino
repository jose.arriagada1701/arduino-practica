/*
 * Por José Arriagada
 * Primer avance de controlador para chip raindrops module, de humedad
 */

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:

  int out = analogRead(A0);

  Serial.print("Salida Analog: ");
  Serial.println(out);

  delay(1800);

}
