/*
 * Por José Arriagada
 * version2 del programa maquina mezcladora, es similar pero con otro sensor para profundidad
 */

#include <Servo.h>

Servo myServo1;
Servo myServo2;
int servo1 = 9;
int servo2= 10;

const int Trigger = 2;   //Pin digital 2 para el Trigger del sensor
const int Echo = 3;   //Pin digital 3 para el Echo del sensor

void setup() {
  myServo1.attach(servo1);//derecho
  myServo2.attach(servo2);//izquierdo
  pinMode(Trigger, OUTPUT); //pin como salida
  pinMode(Echo, INPUT);  //pin como entrada
  digitalWrite(Trigger, LOW);//Inicializamos el pin con 0
  Serial.begin(9600); // open a serial connection to your computer
}

void mover(){

    Serial.println("Empezando a mover los motores");
    moverServo1(180,3);
    moverServo2(180,3);
    Serial.println("Fin de mover los motores");

    
  }
void moverServo1(int a,int ts){
    myServo1.write(a);
    delay(ts*1000); 
    myServo1.write(90);
    delay(ts*1000); 
  }
void moverServo2(int a,int ts){
    myServo2.write(a);
    delay(ts*1000); 
    myServo2.write(90);
    delay(ts*1000); 
  }

void loop() {

  long t; //timepo que demora en llegar el eco
  long d; //distancia en centimetros

  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          //Enviamos un pulso de 10us
  digitalWrite(Trigger, LOW);
  
  t = pulseIn(Echo, HIGH); //obtenemos el ancho del pulso
  d = t/59;             //escalamos el tiempo a una distancia en cm
  
  Serial.print("Distancia: ");
  Serial.print(d);      //Enviamos serialmente el valor de la distancia
  Serial.print("cm");
  Serial.println();
  delay(100); 

  if(d==0){
    mover();
    }
}
  
  
